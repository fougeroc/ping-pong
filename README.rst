Provides a set of python scripts to compute and draw limit sets and related geometric sets for hypergeometric equations.

Examples
--------

Compute limit set for example N=4 in [Fou19] with 4 iterations::

        $ ./frenet 4 --iterations=4

Compute the associated Brav--Thomas cones::

        $ ./frenet 4 --cones

Display help::

        $ ./frenet --help

References
----------

        - [Fou19] - Fougeron. Parabolic degrees and lyapunov exponents for hypergeometric local systems. Experimental Mathematics, doi: 10.1080/10586458.2019.1580632 (2019). arXiv:1701.08387
