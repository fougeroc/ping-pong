import numpy as np
import mayavi.mlab as mlab
import os

def plot_curve(N, it, full=False, cones=False, matrix=False, random_matrix=False, thickness=None, show=True, opacity=.2):
    r = N if N % 2 else N
    filename = '%i_%i.npy'%(N, it)
    if not os.path.isfile('res/' + filename):
        from boundary import init_family
        import time
        B = init_family(N)
        for i in range(it):
            start_time = time.time()
            print("--- step %i ---" % (i+1))
            B.step()
            print("    %.3f seconds     %i points" % (time.time() - start_time, len(B.image)))

        np.save("res/%i_%i" % (N, it), np.array(B.image))

    dat = np.load("res/%s"%filename, allow_pickle=True)

    if random_matrix:
        print("Looking for a nice random matrix")
        D = [max([abs(v[i]/v[3]) for v in dat if not v[3] == 0]) for i in range(3)]
        res = [1e4]
        j = 0

        while max(res) > 1e3 and j < 1000:
            M = np.random.uniform(-1, 1, (4, 4))
            table = []
            res = []
            coord = 3
            for i in range(len(dat)):
                v = M.dot(dat[i])
                w = []
                norm = 0
                if abs(v[coord]) != 0:
                    for i in range(4):
                        if i != coord:
                            w.append(v[i]/v[coord])
                            norm += (v[i]/v[coord])**2
                res.append(norm)
            j += 1

        if j == 1000:
            print("Warning: could not find a nice random matrix.")
        else:
            print("Found a nice random matrix")

        matrix = True
        np.save("res/matrix_%i"%(N), M)
        print(matrix)

    if os.path.isfile('res/matrix_%i.npy'%(N)) and matrix:
        M = np.load('res/matrix_%i.npy'%(N) , allow_pickle=True)
        print(M)
    else:
        M = np.identity(4)

    table = []
    coord = 3
    prev = [10**7]*3
    a = int(len(dat)//(2.*r)+1)
    if full :
        print("Whole Frenet curve : %i points."%(len(dat)))
    else:
        print("Curve between 0 and 1 : %i points out of %i."%(a, len(dat)))

    v0 = M.dot(dat[0])
    for i in range(len(dat) if full else a):
        v = M.dot(dat[i])
        if i>0 and max(abs(v-v0)) < 1e-4 :
            print(str(i) + ' comes back to origin')
        w = []
        norm = 0
        if abs(v[coord]) != 0:
            for i in range(4):
                if i != coord:
                    w.append(v[i]/v[coord])
                    norm += (v[i]/v[coord])**2
            if norm < 10000 :
                # if abs(sum((v[i]-prev[i])**2 for i in range(3))) > .1:
                table.append(w)
                prev = w

    v0, v1, v2= [v[0] for v in table], [v[1] for v in table], [v[2] for v in table]

    thickness = .0001*float(thickness)
    mlab.figure(bgcolor=(1,1,1), size=(1600,1600))
    mlab.plot3d(v0, v1, v2, tube_radius=thickness, color=(1,0,0))

    # starting point in green
    p = M.dot(dat[0])
    t = p[coord]
    mlab.points3d(p[0]/t, p[1]/t, p[2]/t,  color=(0,1,0), scale_factor=.005)

    # end point in blue
    p = M.dot(dat[a-1])
    t = p[coord]
    mlab.points3d(p[0]/t, p[1]/t, p[2]/t,  color=(0,0,1), scale_factor=.005)

    if cones:
        if os.path.isfile('res/cones_%i.npy'):
            C = np.load('res/cones_%i.npy'%(N), allow_pickle=True)
        else:
            from boundary import init_family
            from math import cos, sin, pi
            mu1, mu2 = (N-3)/float(2*N), (N-1)/float(2*N)
            c1, s1 = cos(2*pi*mu1), sin(2*pi*mu1)
            c2, s2 = cos(2*pi*mu2), sin(2*pi*mu2)
            k = 2*(1-c1) + 2*(1-c2)
            d = 4*(1-c1)*(1-c2)

            f = (c1 - 1)*(c2 + 2) - 3*(c2 - 1)   # > 0
            g = (c1 - 1)*(c2 - 1) + 3*(c2 - c1)  # < 0 for N < 7; > 0 otherwise
            h = (c1 - 1)*(c2 + 2) + 3*(c2 - 1)   # < 0

            U = np.matrix([[1,-1,0,0],[0,1,0,0],[-d,0,1,0],[-d,k,1,1]])
            Np = U-np.identity(4)
            P = Np-Np*Np/2+Np*Np*Np/3
            C = np.identity(4)
            v = np.matrix([0,1,0,0]).transpose()
            for i in range(4):
                A = (-P)**i*v
                for j in range(4):
                    C[j,i] = A[j]

            C = np.matrix(
            [(0, -1/24.*f*s1*s2/(c2-1), 0, 1/2.*(c1 - 1)*s1*s2),
             (1/24.*g*s2/(c2 - 1), 1/24*h*(c1-1)*s2/(c2 - 1), 1/2.*(c1 - 1)*s2, -1/2.*(c1 - 1)**2*s2),
             (0, 1/24.*g*s1*s2/(c1-1), 0, -1/2.*(c2 - 1)*s1*s2),
             (-1/24.*f*s1/(c1 - 1), -1/24.*h*(c2-1)*s1/(c1 - 1), -1/2.*(c2 - 1)*s1, 1/2.*(c2 - 1)**2*s1)])

            #C = np.matrix(
            #[(0, -(c1 - 1)**2*(c2 + 2)*s1*s2, 0, (c1 - 1)*s1*s2),
            # (-(c1 - 1)**2*s2, (c1*c2 + 2*c1 + 2*c2 - 5)*(c1 - 1)**2*s2, (c1 - 1)*s2, -(c1 - 1)**2*s2),
            # (0, (c1 + 2)*(c2 - 1)**2*s1*s2, 0, -(c2 - 1)*s1*s2),
            # ((c2 - 1)**2*s1, -(c1*c2 + 2*c1 + 2*c2 - 5)*(c2 - 1)**2*s1, -(c2 - 1)*s1, (c2 - 1)**2*s1)]
            #)

            B = init_family(N)
            F = [C, B.ref*C]
            np.save("res/cones_%i" % (N), np.array(C))

    if cones and not full:
        C = np.array(F[1])
        plot_cone(M.dot(C), opacity)

    if cones and full:
        B = init_family(N)
        for _ in range(B.r-1):
            F.append(B.rot*F[-2])
            F.append(B.rot*F[-2])

        triangles = [(i, (i+1) % 4, (i+2) % 4) for i in range(4)]
        for C in F:
            C = M.dot(C)
            t = False
            for i in range(4):
                if abs(C[coord,i]) < 1e-10:
                    t = True
                    print ('jump')
                C[:,i] = C[:,i]/C[coord,i]
            if t: continue
            plot_cone(np.array(C), opacity)

    if show:
        mlab.show()

def plot_cone(M, opacity=.2):
    coord=3
    for i in range(4):
        M[:,i] = M[:,i]/M[coord,i]
    x = M[0,:]
    y = M[1,:]
    z = M[2,:]
    # the triangle included in the invariant plane for T0 is drawn in green.
    mlab.triangular_mesh(x[1:], y[1:], z[1:],[(i, (i+1) % 3, (i+2) % 3) for i in range(3)], representation='surface', color=(0, 1, 0), tube_sides=15, opacity=opacity)
    mlab.triangular_mesh(x[np.arange(4)!=1], y[np.arange(4)!=1], z[np.arange(4)!=1],[(i, (i+1) % 3, (i+2) % 3) for i in range(3)], representation='surface', color=(0, 0, 1), tube_sides=15, opacity=opacity)
    mlab.triangular_mesh(x[np.arange(4)!=2], y[np.arange(4)!=2], z[np.arange(4)!=2],[(i, (i+1) % 3, (i+2) % 3) for i in range(3)], representation='surface', color=(1, 0, 0), tube_sides=15, opacity=opacity)
    mlab.triangular_mesh(x[np.arange(4)!=3], y[np.arange(4)!=3], z[np.arange(4)!=3],[(i, (i+1) % 3, (i+2) % 3) for i in range(3)], representation='surface', color=(1, 1, 0), tube_sides=15, opacity=opacity)
