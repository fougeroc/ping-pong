from math import cos, sin, pi
from numpy import matrix

class Boundary(object):
    def __init__(self, R01, Ri0, R1i, v0, v1, v2, w0, r):
        """ We assume (R1i*Ri0)^r = 1 """
        self.rot = R1i*Ri0
        self.inv = R01
        self.ref = R1i
        self.r = r
        self.k = 1

        """ Compute fixed point for T0, plane and hyperplane.
            We will always consider the images associated to rationals
            between [0, 1/(2*r)]"""
        self.matrix = ['.v0','.w0']
        self.image = [v0, w0]
        self.reflect()
        self.rotate()

    def rotate(self):
        r, k = self.r, self.k
        N = len(self.image) #(2*r-1)**(k-1)*2
        image = list(range((N-1)*r))
        matrix = list(range((N-1)*r))
        for j in range(N-1):
            # test if we computed it in a previous step
            aux = self.image[j]
            image[j] = aux
            matrix[j] = self.matrix[j]
            for i in range(1,r):
                aux = self.rot * aux
                image[j + i*(N-1)] = aux
                matrix[j + i*(N-1)] = ('R^' + str(i) if i>1 else 'R') + matrix[j]
        self.image = image
        self.matrix = matrix

    def invert(self):
        r, k = self.r, self.k
        N = len(self.image)
        D = N//(2*self.r) + 1 # number of points between v0 and w0 (included)
        M = N-D+2             # number of points between w0 and v0 (included)
        image = list(range(M))
        image[0] = self.image[0]
        image[M-1] = self.image[D-1]
        matrix = list(range(M))
        matrix[0] = self.matrix[0]
        matrix[M-1] = self.matrix[D-1]
        for j in range(M-2):
            image[M-2-j] = self.inv * self.image[D+j]
            matrix[M-2-j] = 'I' + self.matrix[D+j]
        self.image = image
        self.matrix = matrix

    def reflect(self):
        r, k = self.r, self.k
        N = len(self.image)
        M = 2*N-1
        image = list(range(M))
        image[:N] = self.image
        matrix = list(range(M))
        matrix[:N] = self.matrix
        for j in range(N-1):
            image[M-1-j] = self.ref * self.image[j]
            matrix[M-1-j] = 'S' + self.matrix[j]
        self.image = image
        self.matrix = matrix

    def step(self, i=1):
        for _ in range(i):
            self.invert()
            self.k = self.k + 1
            self.reflect()
            self.rotate()


def R1i(mu1, mu2):
    return matrix([[-1 , 0 , 0 , 0 ],[ 0 , 1 , 0 , 0 ],[ 0 , 0 , -1 , 0 ],[ 0 , 0 , 0 , 1]])

def Ri0(mu1,mu2):
    c1, c2, s1, s2 = cos(2*pi*mu1), cos(2*pi*mu2), sin(2*pi*mu1), sin(2*pi*mu2)
    return matrix([(-c1, s1, 0, 0),
                   ( s1, c1, 0, 0),
                   (0, 0, -c2, s2),
                   (0, 0,  s2, c2)])

def R01(mu1,mu2):
    c1, c2, s1, s2 = cos(2*pi*mu1), cos(2*pi*mu2), sin(2*pi*mu1), sin(2*pi*mu2)
    return matrix([(-1, 0, 0, 0),
                   (-2*(c1 - 1)**2/((c1 - c2)*s1), 1, -2*(c1 - 1)**2/((c1 - c2)*s1), 0),
                   (0, 0, -1, 0),
                   (2*(c2 - 1)**2/((c1 - c2)*s2), 0, 2*(c2 - 1)**2/((c1 - c2)*s2), 1)])

def v0(mu1,mu2):
    c1, c2, s1, s2 = cos(2*pi*mu1), cos(2*pi*mu2), sin(2*pi*mu1), sin(2*pi*mu2)
    # left kernel
    # return vector((1, -s1/(c1 - 1), 1, -s2/(c2 - 1)))
    # right kernel
    return matrix([(c1-1)*s2*s1, (c1-1)**2*s2, -(c2 - 1)*s1*s2, -(c2 - 1)**2*s1]).transpose()

def v1(mu1,mu2):
    c1, c2, s1, s2 = cos(2*pi*mu1), cos(2*pi*mu2), sin(2*pi*mu1), sin(2*pi*mu2)
    # left kernel
    # return vector ((0, (c2-1)*s1, (c1-1)*(c1-c2), (c1-1)*s2))
    # right kernel
    return matrix([0, s2*(c1-1), 0, -s1*(c2 - 1)]).transpose()

def v2(mu1,mu2):
    c1, c2, s1, s2 = cos(2*pi*mu1), cos(2*pi*mu2), sin(2*pi*mu1), sin(2*pi*mu2)
    # left kernel
    # return vector((0, 0, c2-1, -s2))
    # right kernel
    return matrix([0, 0, s2, (c2 - 1)]).transpose()

def vv1(mu1,mu2):
    c1, c2, s1, s2 = cos(2*pi*mu1), cos(2*pi*mu2), sin(2*pi*mu1), sin(2*pi*mu2)
    return matrix([0, s2*(c1-1)**2, 0, -s1*(c2-1)**2]).transpose()

def init_family(N):
    r = int(N if (N % 2) else N)
    N = float(N)
    mu1 = float((N-3)/(2*N))
    mu2 = float((N-1)/(2*N))
    return Boundary(R01(mu1,mu2), Ri0(mu1,mu2), R1i(mu1,mu2), v0(mu1,mu2), v1(mu1,mu2), v2(mu1,mu2), vv1(mu1,mu2), r)
